SELECT
ic.category_name,
sum(i.item_price) as total_price

FROM
item i

INNER JOIN
item_category ic

ON
i.category_id = ic.category_id

GROUP BY
category_name

ORDER BY
total_price DESC;
